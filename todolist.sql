CREATE DATABASE todolist DEFAULT CHARACTER SET utf8;

USE todolist;

CREATE TABLE user(
id SERIAL PRIMARY KEY AUTO_INCREMENT,
name varchar(255) NOT NULL,
email varchar(255) UNIQUE NOT NULL,
password varchar(255) NOT NULL
);

USE todolist;
CREATE TABLE todo(
id SERIAL PRIMARY KEY AUTO_INCREMENT,
title varchar(255) NOT NULL,
user_id BIGINT,
FOREIGN KEY(user_id) REFERENCES user(id)
);

INSERT INTO user(
    id,
    name,
    email,
    password
)
VALUES(
    'user01',
    '�ӓ��V',
    'kurumizawa.kouhei.likeit@gmail.com',
    'password' 
);

